package main

import (
	"os"

	"gitee.com/simonxie979/skymeta"
)

func main() {
	if len(os.Args) < 2 {
		panic("config file not special")
	}

	skymeta.Init(os.Args[1])
	defer skymeta.Shutdown()

	if err := skymeta.NewService(&Service{}); err != nil {
		skymeta.Errorf("Main", "new service failure. err: %v", err)
		return
	}

	skymeta.Run()
}
