package uuid

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestNodeIDLimit(t *testing.T) {
	NewSnowflake(1)
	t.Logf("使用NodeID创建雪花UUID生成器成功")
}

func TestEncodeDecode(t *testing.T) {
	sf := NewSnowflake(0)
	uid := sf.Generate()
	fmt.Println("now", time.Now().Unix())
	timestamp, nodeID, sequence := Decode(uid)

	fmt.Println("final", timestamp, nodeID, sequence)
}

func TestSyncGenerate(t *testing.T) {
	const count = 10000000
	var uuids = make(map[uint64]bool)
	var sf = NewSnowflake(1)
	var start = time.Now()

	for i := 0; i < count; i++ {
		var uuid = sf.Generate()
		if uuids[uuid] {
			t.Fatalf("repeated uuid %v with %v", uuid, i)
			return
		}
		uuids[uuid] = true
	}

	var end = time.Now()
	t.Logf("开始时间:%v", start)
	t.Logf("结束时间:%v", end)
	t.Logf("QPS: %0.2f/s", count/end.Sub(start).Seconds())
}

var UUIDMap = make(map[uint64]bool)
var mutex sync.Mutex

func asyncAccessUUIDMap(uuid uint64) bool {
	mutex.Lock()
	defer mutex.Unlock()

	if UUIDMap[uuid] {
		return false
	}

	UUIDMap[uuid] = true

	return true
}

func asyncGenerate(t *testing.T, sf *Snowflake, wg *sync.WaitGroup, gid int, count int) {
	defer wg.Done()
	for i := 0; i < count; i++ {
		var uuid = sf.Generate()
		if !asyncAccessUUIDMap(uuid) {
			t.Logf("repeated uuid %v with %v", uuid, i)
			return
		}
	}
}

func TestAsyncGenerate(t *testing.T) {
	var threedCount = 20
	var uuidCount = 10000
	var wg sync.WaitGroup
	var sf = NewSnowflake(1)
	for i := 0; i < threedCount; i++ {
		wg.Add(1)
		go asyncGenerate(t, sf, &wg, i, uuidCount)
	}
	wg.Wait()
	if len(UUIDMap) != threedCount*uuidCount {
		t.Fatalf("final uuids count %v not equl to %v", len(UUIDMap), threedCount*uuidCount)
	} else {
		t.Logf("final uuid count %v", len(UUIDMap))
	}
}

func BenchmarkGenerate(b *testing.B) {
	var sf = NewSnowflake(1)

	for i := 0; i < b.N; i++ {
		sf.Generate()
	}
}
