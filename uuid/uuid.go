package uuid

import "github.com/google/uuid"

var Generator *Snowflake

func Init(nodeID uint16) {
	Generator = NewSnowflake(nodeID)
}

func GetGuid() uint64 {
	return Generator.Generate()
}

func GetUuid() string {
	return uuid.New().String()
}
