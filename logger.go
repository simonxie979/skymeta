package skymeta

import (
	"os"

	"gitee.com/simonxie979/skymeta/logutil"
	"gitee.com/simonxie979/skymeta/logutil/port"
)

var (
	logger *logutil.Logger
)

func init_Logger() {
	logPath := os.Getenv("log_path")
	if logPath == "" {
		logPath = "."
	}

	logName := os.Getenv("log_name")
	if logName == "" {
		logName = "skymeta"
	}

	logFormatter := os.Getenv("log_formatter")
	if logFormatter == "" {
		logFormatter = "text"
	}

	logLevel := os.Getenv("log_level")
	if logLevel == "" {
		logLevel = "error"
	}

	logger = logutil.NewLogger(g_Context, logPath, logName, logFormatter)
	logger.SetLevel(logLevel)
}

func exit_Logger() {
	logger.Wait()
	logger = nil
}

// SetLogLevel Set log level
func SetLogLevel(level string) error {
	return logger.SetLevel(level)
}

// AddTagFilter Add a tag filter item
func AddTagFilter(tag string) {
	logger.AddTagFilter(tag)
}

// DelTagFilter Delete a tag filter item
func DelTagFilter(tag string) {
	logger.DelTagFilter(tag)
}

// ResetFilter Clear all tag filter item
func ResetFilter() {
	logger.ResetFilter()
}

// LogServiceCallback Intercepts log data to callback function.
func LogServiceCallback(callback port.LogServiceCallBack) {
	logger.AddHook_LogService(callback)
}

// Panic log
func Panicf(tag, format string, args ...any) {
	logger.Panicf_Skip(1, tag, format, args...)
}

// Fatal log
func Fatalf(tag, format string, args ...any) {
	logger.Fatalf_Skip(1, tag, format, args...)
}

// Error log
func Errorf(tag, format string, args ...any) {
	logger.Errorf_Skip(1, tag, format, args...)
}

// Warning log
func Warnf(tag, format string, args ...any) {
	logger.Warnf_Skip(1, tag, format, args...)
}

// Info log
func Infof(tag, format string, args ...any) {
	logger.Infof_Skip(1, tag, format, args...)
}

// Debug log
func Debugf(tag, format string, args ...any) {
	logger.Debugf_Skip(1, tag, format, args...)
}

// Trace log
func Tracef(tag, format string, args ...any) {
	logger.Tracef_Skip(1, tag, format, args...)
}
