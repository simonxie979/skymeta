package main

import (
	"fmt"

	"gitee.com/simonxie979/skymeta/cluster"
	"gitee.com/simonxie979/skymeta/protocol"
)

type Service struct {
	cluster *cluster.Cluster
}

var service *Service

func LaunchNode() {
	service = new(Service)
	service.cluster = cluster.NewCluster(G_Context, log, NodeID)
	service.cluster.Init(nil, nil, service.OnMessage)

	log.Warnf("Service", "node launch success.")
}

func (s *Service) OnMessage(sessionID uint64, msg *protocol.SSMessage) {
	log.Infof("Service", "recive: %v", msg.Name)
}

func (s *Service) SendMsg() {
	msg := &protocol.SSMessage{}
	msg.Source = uint64(NodeID)
	msg.Name = fmt.Sprintf("消息发自: %d", NodeID)
	service.cluster.BordcastMsg(msg)
}
