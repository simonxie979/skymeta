// logutil包提供一个日志对象
// 日志对象提供一下功能：
//  1、提供 Panic Fatal Error Warn Info Debug Trace 七个日志级别，以及动态设置
//  2、支持指定日志路径与日志文件名
//  3、支持动态设置tag过滤
package logutil

import (
	"context"
	"strings"

	"gitee.com/simonxie979/skymeta/logutil/formatter"

	"github.com/sirupsen/logrus"
)

// 实例化一个日志对象
func NewLogger(ctx context.Context, logPath, logName, logFormatter string) *Logger {
	log := new(Logger)
	log.ctx = ctx
	log.object = logrus.New()
	if logPath != "" {
		log.AddHook_FileLog(logPath, logName)
	}
	log.object.SetLevel(logrus.ErrorLevel)

	var obj logrus.Formatter
	switch strings.ToLower(logFormatter) {
	case "text":
		obj = formatter.NewFormatter_Text()
	case "json":
		obj = formatter.NewFormatter_Json()
	default:
		obj = formatter.NewFormatter_Text()
	}
	log.object.SetFormatter(obj)

	return log
}
