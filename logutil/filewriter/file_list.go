package filewriter

import (
	"os"
	"strings"
	"time"
)

type FileList []os.FileInfo

func NewFileList(prefix string, infoList []os.FileInfo) FileList {
	list := make([]os.FileInfo, 0)
	for _, info := range infoList {
		if strings.HasPrefix(info.Name(), prefix) {
			list = append(list, info)
		}
	}
	return list
}

func (list FileList) Len() int {
	return len(list)
}

func (list FileList) Less(i, j int) bool {
	file1 := strings.Split(list[i].Name(), ".")
	time1, _ := time.Parse(TimeFormat, file1[len(file1)-2])

	file2 := strings.Split(list[j].Name(), ".")
	time2, _ := time.Parse(TimeFormat, file2[len(file2)-2])

	return time1.Unix() > time2.Unix()
}

func (list FileList) Swap(i, j int) {
	list[i], list[j] = list[j], list[i]
}
