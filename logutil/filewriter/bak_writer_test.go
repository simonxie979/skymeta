package filewriter

import (
	"bytes"
	"context"
	"fmt"
	"testing"
	"time"
)

func TestBackupWriter(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	w := NewBakWriter(ctx, "./log", "test")
	w.Serve()
	count := 1000000

	now := time.Now()
	defer func() {
		fmt.Println(time.Since(now).Nanoseconds()/int64(count), " ns/op")
	}()
	for i := 0; i < count; i++ {
		w.Write([]byte(fmt.Sprintf("%d\n", i)))
	}

	cancel()

	for {
		switch w.Done() {
		case true:
			return
		default:
		}
	}
}

func TestBackupWriter_Backup(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	w := NewBakWriter(ctx, "./log", "test")
	w.Serve()

	buf := &bytes.Buffer{}
	buf.WriteString("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890\n")

	go func() {
		for {
			select {
			case <-ctx.Done():
				t.Log("write done")
				return
			default:
				w.Write(buf.Bytes())
			}
		}
	}()

	go func() {
		for {
			select {
			case <-time.After(time.Second * 20):
				cancel()
				t.Log("time done")
				return
			case <-ctx.Done():
				return
			}
		}
	}()

	for {
		switch w.Done() {
		case true:
			return
		default:
		}
	}
}

func BenchmarkBackupWriter(b *testing.B) {
	ctx, cancel := context.WithCancel(context.Background())

	w := NewBakWriter(ctx, "./log", "test")
	w.Serve()

	for i := 0; i < b.N; i++ {
		w.Write([]byte(fmt.Sprintf("%d\n", i)))
	}

	cancel()
	for {
		switch w.Done() {
		case true:
			return
		default:
		}
	}
}
