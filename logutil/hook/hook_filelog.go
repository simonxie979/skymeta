package hook

import (
	"fmt"
	"os"

	"gitee.com/simonxie979/skymeta/logutil/port"

	"github.com/sirupsen/logrus"
)

type FileLog struct {
	writer port.FileWriter
}

func NewHook_FileLog(writer port.FileWriter) logrus.Hook {
	hook := new(FileLog)
	hook.writer = writer
	return hook
}

func (hook *FileLog) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (hook *FileLog) Fire(entry *logrus.Entry) error {
	data, err := entry.Bytes()
	if err != nil {
		fmt.Fprintf(os.Stderr, "FileLog Unable to read entry, %v", err)
		return err
	}

	hook.writer.Write(data)
	return nil
}
