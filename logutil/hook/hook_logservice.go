package hook

import (
	"fmt"
	"os"

	"gitee.com/simonxie979/skymeta/logutil/port"

	"github.com/sirupsen/logrus"
)

type LogService struct {
	callBack port.LogServiceCallBack
}

func NewHook_LogService(callback port.LogServiceCallBack) logrus.Hook {
	hook := new(LogService)
	hook.callBack = callback
	return hook
}

func (hook *LogService) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (hook *LogService) Fire(entry *logrus.Entry) error {
	data, err := entry.Bytes()
	if err != nil {
		fmt.Fprintf(os.Stderr, "FileLog Unable to read entry, %v", err)
		return err
	}

	hook.callBack(data)
	return nil
}
