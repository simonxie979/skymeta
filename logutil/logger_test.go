package logutil

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestLogger(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	log := NewLogger(ctx, "./log", "test", "text")
	log.SetLevel("debug")

	count := 1
	now := time.Now()
	defer func() {
		fmt.Println(time.Since(now).Nanoseconds()/int64(count), " ns/op")
	}()
	for i := 0; i < count; i++ {
		log.Warnf("Test", "Test %d", i)
		log.Errorf("Test", "Test %d", i)
	}

	cancel()

	log.Wait()
}

func TestLogger_Async(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	log := NewLogger(ctx, "./log", "test", "text")
	log.SetLevel("debug")

	total := 1000000
	thread := 100
	now := time.Now()
	defer func() {
		fmt.Println(time.Since(now).Nanoseconds()/int64(total), " ns/op")
	}()

	wg := sync.WaitGroup{}
	for i := 0; i < thread; i++ {
		wg.Add(1)
		go func(index int, count int) {
			defer wg.Done()
			for n := 0; n < count; n++ {
				log.Warnf("Test", "index %2d:%7d", index, n)
			}
		}(i, total/thread)
	}

	wg.Wait()
	cancel()

	log.Wait()
}
