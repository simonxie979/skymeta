package formatter

import (
	"fmt"
	"testing"
	"time"

	"gitee.com/simonxie979/skymeta/logutil/port"

	"github.com/sirupsen/logrus"
)

func newEntry1() *logrus.Entry {
	entry := &logrus.Entry{}
	entry.Time = time.Now()
	entry.Level = logrus.DebugLevel
	entry.Message = "Message1"
	entry.Data = make(logrus.Fields, 6)
	entry.Data[port.TagKey] = port.TagKey
	entry.Data[port.FuncKey] = port.FuncKey
	entry.Data[port.StackTraceKey] = ""

	return entry
}

func newEntry2() *logrus.Entry {
	entry := &logrus.Entry{}
	entry.Time = time.Now()
	entry.Level = logrus.DebugLevel
	entry.Message = "Message2"
	entry.Data = make(logrus.Fields, 6)
	entry.Data[port.TagKey] = port.TagKey
	entry.Data[port.FuncKey] = port.FuncKey
	entry.Data[port.StackTraceKey] = ""

	return entry
}

func Test_LogData(t *testing.T) {
	entry1 := newEntry1()
	entry2 := newEntry2()

	obj1 := logDataPool.Get().(*LogData)
	obj1.Init(entry1)
	data1, err1 := obj1.Json()
	logDataPool.Put(obj1)

	obj2 := logDataPool.Get().(*LogData)
	obj2.Init(entry2)
	data2, err2 := obj2.Json()
	logDataPool.Put(obj2)

	fmt.Println(string(data1), err1)
	fmt.Println(string(data2), err2)
	fmt.Printf("%p %p\n", data1, data2)
}

func TestFormatter_Text(t *testing.T) {
	entry := newEntry1()
	formatter := NewFormatter_Text()
	data, err := formatter.Format(entry)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(string(data))
}

func BenchmarkFormatter_Text(b *testing.B) {
	entry := newEntry1()
	formatter := NewFormatter_Text()
	for i := 0; i < b.N; i++ {
		formatter.Format(entry)
	}
}

func TestFormatter_Json(t *testing.T) {
	entry := newEntry1()
	formatter := NewFormatter_Json()
	data, err := formatter.Format(entry)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(string(data))
}

func BenchmarkFormatter_Json(b *testing.B) {
	entry := newEntry1()
	formatter := NewFormatter_Json()
	for i := 0; i < b.N; i++ {
		formatter.Format(entry)
	}
}
