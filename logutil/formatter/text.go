package formatter

import (
	"github.com/sirupsen/logrus"
)

type TextFormatter struct{}

func NewFormatter_Text() logrus.Formatter {
	return new(TextFormatter)
}

func (f TextFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	obj := logDataPool.Get().(*LogData)
	defer logDataPool.Put(obj)

	obj.Init(entry)
	return obj.Text()
}
