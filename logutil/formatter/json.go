package formatter

import "github.com/sirupsen/logrus"

type JsonFormatter struct{}

func NewFormatter_Json() logrus.Formatter {
	return new(JsonFormatter)
}

func (f JsonFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	obj := logDataPool.Get().(*LogData)
	defer logDataPool.Put(obj)

	obj.Init(entry)
	return obj.Json()
}
