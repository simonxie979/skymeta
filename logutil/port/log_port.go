package port

const (
	TimeFormat    = "2006-01-02 15:04:05.000"
	StackTraceKey = "StackTrace"
	TagKey        = "Tag"
	FuncKey       = "Func"
)

type FileWriter interface {
	Serve()
	Write([]byte)
	Done() bool
}

type LogServiceCallBack func([]byte)
