package skymeta

import (
	"testing"
	"time"

	"gitee.com/simonxie979/skymeta/uuid"
)

func Test_Skymeta(t *testing.T) {
	Init("./example.env")

	t.Log(NodeID)
}

func Test_UUID(t *testing.T) {
	Init("./example.env")

	t.Log("now", time.Now().Unix())

	uid := uuid.GetGuid()
	timestamp, nodeID, sequence := uuid.Decode(uid)

	t.Log("final", uid, timestamp, nodeID, sequence)
}

func Test_Logger(t *testing.T) {
	Init("./example.env")

	Tracef("Test", "Trace")
	Debugf("Test", "Debug")
	Infof("Test", "Info")
	Warnf("Test", "Warning")
	Errorf("Test", "Error")
	Fatalf("Test", "Fatal")
	// Panicf("Test", "Panic") will panic

	g_Cancel()
	logger.Wait()
}
