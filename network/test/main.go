package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

const (
	MODE_SERVER = "SERVER"
	MODE_CLIENT = "CLIENT"
)

var headerSize = 4

var headerBufPool = &sync.Pool{
	New: func() any {
		v := make([]byte, headerSize)
		return &v
	},
}

func main() {
	fmt.Println(os.Getpid())
	fmt.Printf("os.Args: %v\n", os.Args)

	mode := MODE_SERVER
	if len(os.Args) > 1 && strings.ToUpper(os.Args[1]) == MODE_CLIENT {
		mode = MODE_CLIENT
	}

	fmt.Printf("启动模式: %s\n", mode)

	switch mode {
	case MODE_SERVER:
		RunServer()
	case MODE_CLIENT:
		count := 0
		if len(os.Args) > 2 {
			num, err := strconv.Atoi(os.Args[2])
			if err != nil {
				fmt.Printf("线程数读取失败: %v\n", err)
			} else if num > 0 {
				count = num
			}
		}

		// time.Sleep(time.Second * 10)
		RunClient(count)
	default:
		fmt.Printf("无效的连接模式\n")
	}
}
