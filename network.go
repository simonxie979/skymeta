package skymeta

import (
	"context"

	"gitee.com/simonxie979/skymeta/network"
)

func NewCommunicator[T any](ctx context.Context, host network.ICommHost[T]) *network.Communicator[T] {
	return network.NewCommunicator(ctx, host, logger)
}
