package skymeta

import (
	"os"
	"regexp"
	"strconv"

	"gitee.com/simonxie979/skymeta/cluster"
	"gitee.com/simonxie979/skymeta/uuid"
)

var (
	NodeID uint16
	node   *cluster.Cluster
)

func init_NodeID() {
	str := os.Getenv("node_id")
	if !regexp.MustCompile(`^\d+$`).MatchString(str) {
		panic("invalid node_id: " + str)
	}
	num, err := strconv.ParseUint(str, 10, 12)
	if err != nil {
		panic(err)
	}

	NodeID = uint16(num)
	uuid.Init(NodeID)
}

func init_Cluster() {
	node = cluster.NewCluster(g_Context, logger, NodeID)
	node.Init(onConnect, onDisConnect, onMessage)
}

func exit_Cluster() {
	node.Close()
	node = nil
}
